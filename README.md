# gdparse
Python module for parsing Breseq's Genomic Diff (.gd) files.

Currently works with Genomic Diff files output by Breseq version 0.26.1:  
https://github.com/barricklab/breseq/releases/tag/v0.26.1

Original may be (though not verified with author):
https://github.com/phageghost/gdparse/blob/master/gdparse.py
